<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Sede.model.php");
        $obj = new Sede();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Sede"]);
        $arr['where'] = 'LIST';
        $result = $obj->sedeGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('SEDE', $row['sede']);
            $xtpl->assign('ESTADO', $row['estado']);
            $xtpl->assign('MUNICIPIO', $row['municipio']);
            $xtpl->assign('CIUDAD', $row['ciudad']);
            $xtpl->assign('PARROQUIA', $row['parroquia']);
            $xtpl->assign('DIRECCION', $row['direccion']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"], 
            ["btnName"=>"SpreadSheet", "btnClick"=>"location.href='../../../".$_SESSION['myApp']."/reportes/sede_calc/'"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
