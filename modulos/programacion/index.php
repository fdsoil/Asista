<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Programacion.model.php");
        $obj = new Programacion();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Programacion"]);
        $arr['where'] = 'LIST';
        $result = $obj->programacionGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('ID_EVENTO_AUX', $row['id_evento_aux']);
            $xtpl->assign('FECHA', $row['fech_programa']);
            $xtpl->assign('HORA', $obj->timeMilitarToNormal($row['hora_programa']));  
            $xtpl->assign('PROGRAMA', $row['programa']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $xtpl->assign('REPRESENTANTE', $row['representante']);
            $xtpl->assign('DES_EVENTO_AUX', $row['des_evento_aux']);
//            $xtpl->assign('DES_EVENTO_AUX', $row['des_evento_aux']);
//            $xtpl->assign('DES_EVENTO_AUX', $row['des_evento_aux']);
//            $xtpl->assign('DES_EVENTO_AUX', $row['des_evento_aux']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
