function submitInsert(val)
{
    relocate('../organiza_aux/', {});
}

function submitEdit(val)
{
    relocate('../organiza_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}