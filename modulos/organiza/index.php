<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Organiza.model.php");
        $obj = new Organiza();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Organizador"]);
        $arr['where'] = 'LIST';
        $result = $obj->organizaGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('NOMBRE', $row['nombre']);
            $xtpl->assign('OBJETO', $row['objeto']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"], 
            ["btnName"=>"SpreadSheet", "btnClick"=>"location.href='../../../".$_SESSION['myApp']."/reportes/organiza_calc/'"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
