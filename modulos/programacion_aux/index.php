<?php
class SubIndex
{
    public function execute($aReqs)
    {
        require_once("../../../".$_SESSION['myApp']."/class/Programacion.model.php");
        $obj = new Programacion();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $arr['where'] = 'REGIST';
        $aRegist=array_key_exists('id', $_POST)?$obj->extraer_asociativo($obj->programacionGet($arr)):$obj->iniRegist('programacion','asista');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('ID_EVENTO_AUX', $aRegist['id_evento_aux']);
        $xtpl->assign('ID_EVENTO_AUX_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('FECHA', $aRegist['fech_programa']);
        $xtpl->assign('HORA', $obj->timeMilitarToNormal($aRegist['hora_programa']));
        $xtpl->assign('PROGRAMA', $aRegist['programa']);
        $xtpl->assign('PROGRAMA_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('DESCRIPCION', $aRegist['descripcion']);
        $xtpl->assign('REPRESENTANTE', $aRegist['representante']);
        $result = $obj->eventoAuxList();
        while ($row = $obj->extraer_registro($result)) {
            $xtpl->assign('ID_EVENTO_AUX', $row[0]);
            $xtpl->assign('DES_EVENTO_AUX', $row[1]);
            $xtpl->assign('SELECTED_EVENTO_AUX', ($aRegist['id_evento_aux'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.evento_aux');
        }
        $obj->btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "programacion"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
