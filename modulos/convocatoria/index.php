<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Convocatoria.model.php");
        $obj = new Convocatoria();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Convocatoria"]);
        $arr['where'] = 'LIST';
        $result = $obj->convocatoriaGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('ID_EVENTO_AUX', $row['id_evento_aux']);
            $xtpl->assign('DES_EVENTO', $row['des_evento']);
            $xtpl->assign('DES_INVITADO', $row['des_invitado'].'.');
            $xtpl->assign('ID_INVITADO', $row['id_invitado']);
            $xtpl->assign('FECHA_CONVOCATORIA', $row['fecha_convocatoria']);
            $xtpl->assign('HORA_CONVOCATORIA', $row['hora_convocatoria']);
            $xtpl->assign('CONFIRMACION', ($row['confirmacion']==='t')?'SI':'NO');
            $xtpl->assign('FECHA_CONFIRMACION', $row['fecha_confirmacion']);
            $xtpl->assign('HORA_CONFIRMACION', $row['hora_confirmacion']);
            $xtpl->assign('ASISTENCIA', ($row['asistencia']==='t')?'SI':'NO');
            $xtpl->assign('FECHA_ASISTENCIA', $row['fecha_asistencia']);
            $xtpl->assign('HORA_ASISTENCIA', $row['hora_asistencia']);
            $xtpl->assign('OBSERVACION', $row['observacion']);
            $xtpl->assign('DES_EVENTO_AUX', $row['des_evento_aux']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
