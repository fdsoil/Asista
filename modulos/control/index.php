<?php class Index
{
    public function control()
    {
        session_start();
        require_once("../../../".$_SESSION['FDSoil']."/class/Controll.class.php");
        $arr['path']=__DIR__."/../";
        $strFileName = "../../config/app.json";
        file_exists($strFileName) 
            ? $oJSON = json_decode(file_get_contents($strFileName)) 
            : die("File not Found " . $strFileName);
        $arr['filesName']= $oJSON->filesName;
        $obj = new Controll($arr);
        $obj->execute();
    }
}
Index::control();

