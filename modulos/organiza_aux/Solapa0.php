<?php

trait Solapa0
{
    private function _solapa0()
    {
        $xtpl = new XTemplate(__DIR__."/solapa0.html");
        $this->_obj->appShowId($xtpl);
        $xtpl->assign('NOMBRE', $this->_aRegist['nombre']);
        $xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('OBJETO', $this->_aRegist['objeto']);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

