function onOffPanelOrganizaRedSocial() 
{
    toggle('id_panel_organiza_red_social', 'blind',500);
    changeTheObDeniedWritingImg('id_img_organiza_red_social');
    initObjs(document.getElementById('id_panel_organiza_red_social'));
    document.getElementById('id_organiza_red_social').value=0;
}

function valPanelOrganizaRedSocial()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_organiza_red_social');
    if ( validateObjs(document.getElementById('id_panel_organiza_red_social'))) {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioOrganizaRedSocial()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioOrganizaRedSocial()
{
    var oDiv=document.getElementById('id_panel_organiza_red_social');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendOrganizaRedSocialRegister(reqs);
}

function editOrganizaRedSocial(obj)
{
    if(document.getElementById('id_panel_organiza_red_social').style.display=='none'){
        onOffPanelOrganizaRedSocial();
    }
    document.getElementById('id_organiza_red_social').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_organiza_red_social");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['red_social'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_red_social'].value = obj.parentNode.parentNode.cells[0].id;
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelOrganizaRedSocial();
}

