function sendOrganizaRedSocialRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_organiza') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_organiza_red_social'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/organiza/organiza_red_social_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendOrganizaRedSocialGet();
            onOffPanelOrganizaRedSocial();
        }
    }
}

function sendOrganizaRedSocialGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/organiza/organiza_red_social_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabOrganizaRedSocial(response);
        valBtnClose();
    }
}

function sendOrganizaRedSocialDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/organiza/organiza_red_social_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendOrganizaRedSocialGet();
    }
}

