function sendInvitadoRedSocialRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_invitado') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_invitado_red_social'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/invitado/invitado_red_social_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendInvitadoRedSocialGet();
            onOffPanelInvitadoRedSocial();
        }
    }
}

function sendInvitadoRedSocialGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/invitado/invitado_red_social_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabInvitadoRedSocial(response);
        valBtnClose();
    }
}

function sendInvitadoRedSocialDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/invitado/invitado_red_social_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendInvitadoRedSocialGet();
    }
}

