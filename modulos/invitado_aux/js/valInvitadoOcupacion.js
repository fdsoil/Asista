function onOffPanelInvitadoOcupacion() 
{
    toggle('id_panel_invitado_ocupacion', 'blind',500);
    changeTheObDeniedWritingImg('id_img_invitado_ocupacion');
    initObjs(document.getElementById('id_panel_invitado_ocupacion'));
    document.getElementById('id_invitado_ocupacion').value=0;
}

function valPanelInvitadoOcupacion()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_invitado_ocupacion');
    if ( validateObjs(document.getElementById('id_panel_invitado_ocupacion'))) {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioInvitadoOcupacion()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioInvitadoOcupacion()
{
    var oDiv=document.getElementById('id_panel_invitado_ocupacion');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendInvitadoOcupacionRegister(reqs);
}

function editInvitadoOcupacion(obj)
{
    if(document.getElementById('id_panel_invitado_ocupacion').style.display=='none'){
        onOffPanelInvitadoOcupacion();
    }
    document.getElementById('id_invitado_ocupacion').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_invitado_ocupacion");
    var oInputs=oPanel.getElementsByTagName("input");
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_ocupacion'].value = obj.parentNode.parentNode.cells[0].id;
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelInvitadoOcupacion();
}

