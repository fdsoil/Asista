function onOffPanelInvitadoRedSocial() 
{
    toggle('id_panel_invitado_red_social', 'blind',500);
    changeTheObDeniedWritingImg('id_img_invitado_red_social');
    initObjs(document.getElementById('id_panel_invitado_red_social'));
    document.getElementById('id_invitado_red_social').value=0;
}

function valPanelInvitadoRedSocial()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_invitado_red_social');
    if ( validateObjs(document.getElementById('id_panel_invitado_red_social'))) {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioInvitadoRedSocial()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioInvitadoRedSocial()
{
    var oDiv=document.getElementById('id_panel_invitado_red_social');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendInvitadoRedSocialRegister(reqs);
}

function editInvitadoRedSocial(obj)
{
    if(document.getElementById('id_panel_invitado_red_social').style.display=='none'){
        onOffPanelInvitadoRedSocial();
    }
    document.getElementById('id_invitado_red_social').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_invitado_red_social");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['red_social'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_red_social'].value = obj.parentNode.parentNode.cells[0].id;
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelInvitadoRedSocial();
}

