function sendInvitadoOcupacionRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_invitado') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_invitado_ocupacion'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/invitado/invitado_ocupacion_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendInvitadoOcupacionGet();
            onOffPanelInvitadoOcupacion();
        }
    }
}

function sendInvitadoOcupacionGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/invitado/invitado_ocupacion_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabInvitadoOcupacion(response);
        valBtnClose();
    }
}

function sendInvitadoOcupacionDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/invitado/invitado_ocupacion_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendInvitadoOcupacionGet();
    }
}

