function fillTabInvitadoOcupacion(aObjs)
{

    var tbl = document.getElementById('id_tab_invitado_ocupacion');
    deleteAllRowsTable('id_tab_invitado_ocupacion',1);

    if (!tbl.tBodies[0]) {
        var tbody = document.createElement('tbody');
        tbody.setAttribute('style', 'display: block');
        tbody.setAttribute('style', 'width: 100%');
        tbl.appendChild(tbody);
    }

    if (aObjs.length !== 0) {
        for (var i = 0; i < aObjs.length; i++)
            tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
        //setAttributeTD('id_tab_invitado_ocupacion','align','left', 1, 3, 1, aObjs.length+1);
        paintTRsClearDark('id_tab_invitado_ocupacion');
    }

    function fillTabAux(row)
    {

        var oTr = document.createElement('tr');
        oTr.id = row.id;
        var aTd = [];

        aTd[0] = document.createElement('td');
        aTd[0].id = row.id_ocupacion
        aTd[0].appendChild(document.createTextNode(row.des_ocupacion));
        oTr.appendChild(aTd[0]);

        var oImgEdit = document.createElement('img');
        oImgEdit.setAttribute('class', 'accion');
        oImgEdit.setAttribute('src', '../../../../../'+appOrg+'/img/edit.png');
        oImgEdit.setAttribute('title', 'Editar datos...');
        oImgEdit.setAttribute('onclick',"editInvitadoOcupacion(this);valBtnClose();");
        aTd[1] = document.createElement('td');
        aTd[1].align = 'center';
        aTd[1].appendChild(oImgEdit);

        var oImgDelete = document.createElement('img');
        oImgDelete.setAttribute('class', 'accion');
        oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/cross.png');
        oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
        oImgDelete.setAttribute('onclick',"sendInvitadoOcupacionDelete("+row.id+");valBtnClose();");
        aTd[1].appendChild(oImgDelete);
        oTr.appendChild(aTd[1]);

        return oTr;

    }

}