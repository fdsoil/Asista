<?php

trait Solapa0
{
    private function _solapa0()
    {
        $xtpl = new XTemplate(__DIR__."/solapa0.html");
        $this->_obj->appShowId($xtpl);
        $xtpl->assign('SELECTED_V',(substr($this->_aRegist['cedula'],0,1)=='V')?'selected':'');
        $xtpl->assign('SELECTED_E',(substr($this->_aRegist['cedula'],0,1)=='E')?'selected':'');
        $xtpl->assign('CEDULA', substr($this->_aRegist['cedula'],1));
        $xtpl->assign('NACIONALIDAD_READONLY1', array_key_exists('id', $_POST) ? 'onfocus="this.blur()"' : '');
        $xtpl->assign('NACIONALIDAD_READONLY2', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('CEDULA_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('NOMBRE', $this->_aRegist['nombre']);
        $xtpl->assign('APELLIDO', $this->_aRegist['apellido']);
        $xtpl->assign('SELECTED_SEXO_MASCULINO', ($this->_aRegist['sexo'] == 'M') ? 'checked' : '');
        $xtpl->assign('SELECTED_SEXO_FEMENINO', ($this->_aRegist['sexo'] == 'F') ? 'checked' : '');
        if (array_key_exists('id', $_POST)) {
            $ubiGeoCod="[[\"0\",\"".$this->_aRegist['estado']   ."\"],
                         [\"0\",\"".$this->_aRegist['municipio']."\"],
                         [\"0\",\"".$this->_aRegist['ciudad']   ."\"],
                         [\"0\",\"".$this->_aRegist['parroquia']."\"]]";
            $ubiGeoTex="Estado: ".$this->_aRegist['estado']   ."\nMunicipio: ".$this->_aRegist['municipio']
                    ."\nCiudad: ".$this->_aRegist['ciudad']."\nParroquia: ".$this->_aRegist['parroquia'];
        } else {
            $ubiGeoCod='[[0,""],[0,""],[0,""],[0,""]]';
            $ubiGeoTex="";           
        }
        $xtpl->assign('UBI_GEO_COD', $ubiGeoCod);
        $xtpl->assign('UBI_GEO_TEX', $ubiGeoTex);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

