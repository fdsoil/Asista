<?php

include_once(__DIR__."/Solapa0.php");
include_once(__DIR__."/Solapa1.php");
include_once(__DIR__."/Solapa2.php");

class SubIndex
{
    use Solapa0, Solapa1, Solapa2;

    private $_obj;
    private $_aRegist;
    private $_aView;

    public function __construct()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Invitado.model.php");
        $this->_obj = new Invitado();
        $this->_aView['include'] = $this->_obj->getFileJSON(__DIR__."/js/include.json");
        $this->_aView['userData'] = $this->_obj->usuarioData();
        $this->_aView['load'] = array_key_exists('id', $_POST)?$_POST['id']:0;
        $arr['where'] = 'REGIST';
        $this->_aRegist=array_key_exists('id', $_POST)
            ? $this->_obj->extraer_asociativo($this->_obj->invitadoGet($arr))
            : $this->_obj->iniRegist('invitado','asista');
    }

    public function execute()
    {
        $xtpl = new XTemplate(__DIR__."/view.html");
        $this->_obj->appShowId($xtpl);
        if ($this->_aRegist['id'] == 0) {
            $xtpl->assign('TAB_NONE_BLOCK1', 'none');
            $xtpl->assign('TAB_NONE_BLOCK2', 'none');
            $xtpl->assign('BOTONES_NONE_BLOCK', 'none');
        }
        $xtpl->assign('ID', $this->_aRegist['id']);
        //$xtpl->assign('CODIGO', $this->_aRegist['codigo']);
        //$xtpl->assign('FECHA', $this->_aRegist['fecha_registro']);
        $aSolapa[0] = self::_solapa0();
        $aSolapa[1] = self::_solapa1();
        $aSolapa[2] = self::_solapa2();
        $this->_obj->bldSolapas($xtpl, $aSolapa);
        $this->_obj->btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "invitado"],
                                            ["btnName" => "Save", "btnClick"=> "valEnvio(1);"]
//                                            ,["btnName" => "Close", "btnClick"=> "valEnvio(2);", "btnLabel" => "Invitado", "btnDisplay" => "none"]
]);
        $xtpl->parse('main');
        $this->_aView['content'] = $xtpl->out_var('main');
        return $this->_aView;
    }
}

