<?php

trait Solapa2
{
    private function _solapa2()
    {
        $xtpl = new XTemplate(__DIR__."/solapa2.html");
        $this->_obj->appShowId($xtpl);
        $result = $this->_obj->ocupacionList();
        while ($row = $this->_obj->extraer_registro($result)) {
            $xtpl->assign('ID_OCUPACION', $row[0]);
            $xtpl->assign('DES_OCUPACION', $row[1]);
            $xtpl->parse('main.ocupacion');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = $this->_obj->invitadoOcupacionGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('ID_OCUPACION', $arr['id_ocupacion']);
                $xtpl->assign('DES_OCUPACION', $arr['des_ocupacion']);
                $xtpl->parse('main.tab_invitado_ocupacion');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}