<?php

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new XTemplate(__DIR__."/solapa1.html");
        $this->_obj->appShowId($xtpl);
        $result = $this->_obj->redSocialList();
        while ($row = $this->_obj->extraer_registro($result)) {
            $xtpl->assign('ID_RED_SOCIAL', $row[0]);
            $xtpl->assign('DES_RED_SOCIAL', $row[1]);
            $xtpl->parse('main.red_social');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = $this->_obj->sedeRedSocialGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('RED_SOCIAL', $arr['red_social']);
                $xtpl->assign('ID_RED_SOCIAL', $arr['id_red_social']);
                $xtpl->assign('DES_RED_SOCIAL', $arr['des_red_social']);
                $xtpl->parse('main.tab_sede_red_social');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}