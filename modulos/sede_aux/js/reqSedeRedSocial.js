function sendSedeRedSocialRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_sede') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_sede_red_social'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/sede/sede_red_social_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendSedeRedSocialGet();
            onOffPanelSedeRedSocial();
        }
    }
}

function sendSedeRedSocialGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/sede/sede_red_social_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabSedeRedSocial(response);
        valBtnClose();
    }
}

function sendSedeRedSocialDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/sede/sede_red_social_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendSedeRedSocialGet();
    }
}

