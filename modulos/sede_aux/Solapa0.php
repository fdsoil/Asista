<?php

trait Solapa0
{
    private function _solapa0()
    {
        $xtpl = new XTemplate(__DIR__."/solapa0.html");
        $this->_obj->appShowId($xtpl);
        $xtpl->assign('SEDE', $this->_aRegist['sede']);
        $xtpl->assign('SEDE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        if (array_key_exists('id', $_POST)) {
            $ubiGeoCod="[[\"0\",\"".$this->_aRegist['estado']   ."\"],
                         [\"0\",\"".$this->_aRegist['municipio']."\"],
                         [\"0\",\"".$this->_aRegist['ciudad']   ."\"],
                         [\"0\",\"".$this->_aRegist['parroquia']."\"]]";
            $ubiGeoTex="Estado: ".$this->_aRegist['estado']   ."\nMunicipio: ".$this->_aRegist['municipio']
                    ."\nCiudad: ".$this->_aRegist['ciudad']."\nParroquia: ".$this->_aRegist['parroquia'];
        } else {
            $ubiGeoCod='[[0,""],[0,""],[0,""],[0,""]]';
            $ubiGeoTex="";           
        }
        $xtpl->assign('UBI_GEO_COD', $ubiGeoCod);
        $xtpl->assign('UBI_GEO_TEX', $ubiGeoTex);
        $xtpl->assign('DIRECCION', $this->_aRegist['direccion']);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

