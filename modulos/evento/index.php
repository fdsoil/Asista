<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Evento.model.php");
        $obj = new Evento();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Evento"]);
        $arr['where'] = 'LIST';
        $result = $obj->eventoGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $xtpl->assign('FECHA_AUX', $row['fecha_aux']);
            $xtpl->assign('HORA_AUX', $obj->timeMilitarToNormal($row['hora_aux']));  
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
