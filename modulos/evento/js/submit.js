function submitInsert(val)
{
    relocate('../evento_aux/', {});
}

function submitEdit(val)
{
    relocate('../evento_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}