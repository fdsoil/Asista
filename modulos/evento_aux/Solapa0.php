<?php

trait Solapa0
{
    private function _solapa0()
    {
        $xtpl = new XTemplate(__DIR__."/solapa0.html");
        $this->_obj->appShowId($xtpl);
        $xtpl->assign('DESCRIPCION', $this->_aRegist['descripcion']);
        $xtpl->assign('DESCRIPCION_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('FECHA_AUX', $this->_aRegist['fecha_aux']);
        $xtpl->assign('HORA_AUX', $this->_obj->timeMilitarToNormal($this->_aRegist['hora_aux']));
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

