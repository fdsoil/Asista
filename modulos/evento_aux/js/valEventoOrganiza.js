function onOffPanelEventoOrganiza() 
{
    toggle('id_panel_evento_organiza', 'blind',500);
    changeTheObDeniedWritingImg('id_img_evento_organiza');
    initObjs(document.getElementById('id_panel_evento_organiza'));
    document.getElementById('id_evento_organiza').value=0;
}

function valPanelEventoOrganiza()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_evento_organiza');
    if ( validateObjs(document.getElementById('id_panel_evento_organiza'))) {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioEventoOrganiza()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioEventoOrganiza()
{
    var oDiv=document.getElementById('id_panel_evento_organiza');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendEventoOrganizaRegister(reqs);
}

function editEventoOrganiza(obj)
{
    if(document.getElementById('id_panel_evento_organiza').style.display=='none'){
        onOffPanelEventoOrganiza();
    }
    document.getElementById('id_evento_organiza').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_evento_organiza");
    var oInputs=oPanel.getElementsByTagName("input");
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_organiza'].value = obj.parentNode.parentNode.cells[0].id;
    //var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelEventoOrganiza();
}

