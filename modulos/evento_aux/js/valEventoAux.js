function onOffPanelEventoAux() 
{
    toggle('id_panel_evento_aux', 'blind',500);
    changeTheObDeniedWritingImg('id_img_evento_aux');
    //initObjs(document.getElementById('id_panel_evento_aux'));
    document.getElementById('id_sede').value=null;
    document.getElementById('idfecha').value=dateOfToday('YYYYMMDD', '-');
    document.getElementById('hora').value=currentTime();
    document.getElementById('id_evento_aux').value=0;
    valPanelEventoAux();
}

function valPanelEventoAux()
{
    var ruta="../../../../"+appOrg+"/img/";
    var objImgAdd=document.getElementById('id_img_add_evento_aux');
    if ( validateObjs(document.getElementById('id_panel_evento_aux'))) {
        objImgAdd.src=ruta+'addnew.gif';
        objImgAdd.setAttribute('onclick','valEnvioEventoAux()');
    } else {
        objImgAdd.src=ruta+'addnew_off.png';
        objImgAdd.setAttribute('onclick','');
   }
}

function valEnvioEventoAux()
{
    var oDiv=document.getElementById('id_panel_evento_aux');
    var reqs = "id="+document.getElementById('id').value;
    reqs += "&"+request(oDiv);
    sendEventoAuxRegister(reqs);
}

function editEventoAux(obj)
{
    if(document.getElementById('id_panel_evento_aux').style.display=='none'){
        onOffPanelEventoAux();
    }
    document.getElementById('id_evento_aux').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_evento_aux");
    var oInputs=oPanel.getElementsByTagName("input");
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_sede'].value = obj.parentNode.parentNode.cells[0].id;
    oInputs['fecha'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    oInputs['hora'].value = obj.parentNode.parentNode.cells[2].innerHTML;
    //var oTextAreas=oPanel.getElementsByTagName("textarea");
    valPanelEventoAux();
}

