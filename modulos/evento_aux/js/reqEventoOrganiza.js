function sendEventoOrganizaRegister(reqs) 
{
    var objId = document.getElementById('id');
    var reqs = b64EncodeUnicode('id_evento') + '=' + b64EncodeUnicode(objId.value) + '&' + request(document.getElementById('id_panel_evento_organiza'), true);
    send_ajax('POST', "../../../"+myApp+"/reqs/evento/evento_organiza_register.php", responseAjax, reqs, null, false, true, true);

    function responseAjax(response)
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendEventoOrganizaGet();
            onOffPanelEventoOrganiza();
        }
    }
}

function sendEventoOrganizaGet(bAsync)
{
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    send_ajax('POST', "../../../"+myApp+"/reqs/evento/evento_organiza_get.php", responseAjax, reqs, null, true, bAsync, true);

    function responseAjax(response)
    {
        fillTabEventoOrganiza(response);
        valBtnClose();
    }
}

function sendEventoOrganizaDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            send_ajax("POST", "../../../../../"+myApp+"/reqs/evento/evento_organiza_delete.php", responseAjax, b64EncodeUnicode("id")+"="+b64EncodeUnicode(id), null, false, true, true);
    });

    function responseAjax(response)
    {
        (response != 'B') ? msjAdmin(response) : sendEventoOrganizaGet();
    }
}

