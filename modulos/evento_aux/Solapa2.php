<?php

trait Solapa2
{
    private function _solapa2()
    {
        $xtpl = new XTemplate(__DIR__."/solapa2.html");
        $this->_obj->appShowId($xtpl);
        $result = $this->_obj->organizaList();
        while ($row = $this->_obj->extraer_registro($result)) {
            $xtpl->assign('ID_ORGANIZA', $row[0]);
            $xtpl->assign('DES_ORGANIZA', $row[1]);
            $xtpl->parse('main.organiza');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = $this->_obj->eventoOrganizaGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('ID_ORGANIZA', $arr['id_organiza']);
                $xtpl->assign('DES_ORGANIZA', $arr['des_organiza']);
                $xtpl->parse('main.tab_evento_organiza');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}