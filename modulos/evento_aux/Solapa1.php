<?php

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new XTemplate(__DIR__."/solapa1.html");
        $this->_obj->appShowId($xtpl);
        $result = $this->_obj->sedeList();
        while ($row = $this->_obj->extraer_registro($result)) {
            $xtpl->assign('ID_SEDE', $row[0]);
            $xtpl->assign('DES_SEDE', $row[1]);
            $xtpl->parse('main.sede');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = $this->_obj->eventoAuxGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('FECHA', $arr['fecha']);
                $xtpl->assign('HORA', $arr['hora']);
                $xtpl->assign('ID_SEDE', $arr['id_sede']);
                $xtpl->assign('DES_SEDE', $arr['des_sede']);
                $xtpl->parse('main.tab_evento_aux');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}