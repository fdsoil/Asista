function submitInsert(val)
{
    relocate('../ocupacion_aux/', {});
}

function submitEdit(val)
{
    relocate('../ocupacion_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}