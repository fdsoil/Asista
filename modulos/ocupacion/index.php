<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Ocupacion.model.php");
        $obj = new Ocupacion();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Ocupacion"]);
        $arr['where'] = 'LIST';
        $result = $obj->ocupacionGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}