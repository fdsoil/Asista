<?php
class SubIndex
{
    public function execute($aReqs)
    {
        require_once("../../../".$_SESSION['myApp']."/class/RedSocial.model.php");
        $obj = new RedSocial();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $arr['where'] = 'REGIST';
        $aRegist=array_key_exists('id', $_POST)?$obj->extraer_asociativo($obj->redSocialGet($arr)):$obj->iniRegist('red_social','asista');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('DESCRIPCION', $aRegist['descripcion']);
        //$xtpl->assign('DESCRIPCION_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $obj->btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "red_social"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
