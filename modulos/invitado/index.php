<?php
class SubIndex
{
    public function execute()
    {
        require_once("../../../".$_SESSION['myApp']."/class/Invitado.model.php");
        $obj = new Invitado();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $obj->btnRecordAdd( $xtpl ,["btnRecordName"=>"Invitado"]);
        $arr['where'] = 'LIST';
        $result = $obj->invitadoGet($arr);
        while ($row = $obj->extraer_asociativo($result)){
            $xtpl->assign('CEDULA', $row['cedula']);
            $xtpl->assign('NOMBRE', $row['nombre']);
            $xtpl->assign('APELLIDO', $row['apellido']);
            $xtpl->assign('SEXO', ($row['sexo']=='M')?'MASCULINO':'FEMENINO');
            $xtpl->assign('ESTADO', $row['estado']);
            $xtpl->assign('MUNICIPIO', $row['municipio']);
            $xtpl->assign('CIUDAD', $row['ciudad']);
            $xtpl->assign('PARROQUIA', $row['parroquia']);
            $obj->btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            $obj->btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        $obj->btnsPutPanel( $xtpl ,[["btnName"=>"Exit"], 
            ["btnName"=>"SpreadSheet", "btnClick"=>"location.href='../../../".$_SESSION['myApp']."/reportes/invitado_calc/'"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
