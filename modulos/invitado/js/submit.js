function submitInsert(val)
{
    relocate('../invitado_aux/', {});
}

function submitEdit(val)
{
    relocate('../invitado_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}