function submitInsert(val)
{
    relocate('../red_social_aux/', {});
}

function submitEdit(val)
{
    relocate('../red_social_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}