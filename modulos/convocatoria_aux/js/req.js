function sendInvitadoGet(ced)
{
    if (ced.length>7)
        send_ajax('POST', "../../../"+myApp+"/reqs/convocatoria/invitado_get.php", responseAjax, "ced="+ced, null, true, false);

    function responseAjax(response) 
    {
        if (response!=false){
            document.getElementById('id_invitado').value = response.id;
            document.getElementById('id_nombre_apellido').value = response.nombre+', '+response.apellido;
        } else {
            document.getElementById('id_invitado').value = 0;
            document.getElementById('id_nombre_apellido').value = ', ';
        }
    }
}
