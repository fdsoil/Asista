<?php
class SubIndex
{
    public function execute($aReqs)
    {
        require_once("../../../".$_SESSION['myApp']."/class/Convocatoria.model.php");
        $obj = new Convocatoria();
        $aView['include'] = $obj->getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = $obj->usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        $obj->appShowId($xtpl);
        $arr['where'] = 'REGIST';
        $aRegist=array_key_exists('id', $_POST)?$obj->extraer_asociativo($obj->convocatoriaGet($arr)):$obj->iniRegist('convocatoria','asista');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('ID_EVENTO_AUX', $aRegist['id_evento_aux']);
        $xtpl->assign('ID_EVENTO_AUX_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('ID_INVITADO', $aRegist['id_invitado']);
        $xtpl->assign('CEDULA', substr($aRegist['des_invitado_cedula'],1));
        $xtpl->assign('NACIONALIDAD_READONLY1', array_key_exists('id', $_POST) ? 'onfocus="this.blur()"' : '');
        $xtpl->assign('NACIONALIDAD_READONLY2', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('CEDULA_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        //$xtpl->assign('CEDULA', $aRegist['des_invitado_cedula']);
        $xtpl->assign('NOMBRE_APELLIDO', $aRegist['des_invitado_nombre'].', '.$aRegist['des_invitado_apellido']);
        $xtpl->assign('ID_INVITADO_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('FECHA_CONVOCATORIA', $obj->voltearFecha($aRegist['fecha_convocatoria']));
        $xtpl->assign('HORA_CONVOCATORIA', $obj->timeMilitarToNormal( $aRegist['hora_convocatoria']));
        $xtpl->assign('CONFIRMACION_CHK', $aRegist['confirmacion'] === 't' ? 'checked' : '');
        $xtpl->assign('FECHA_CONFIRMACION', $obj->voltearFecha($aRegist['fecha_confirmacion']));
        $xtpl->assign('HORA_CONFIRMACION', $obj->timeMilitarToNormal( $aRegist['hora_confirmacion']));
        $xtpl->assign('ASISTENCIA_CHK', $aRegist['asistencia'] === 't' ? 'checked' : '');
        $xtpl->assign('FECHA_ASISTENCIA', $obj->voltearFecha($aRegist['fecha_asistencia']));
        $xtpl->assign('HORA_ASISTENCIA', $obj->timeMilitarToNormal( $aRegist['hora_asistencia']));
        $xtpl->assign('OBSERVACION', $aRegist['observacion']);
        $result = $obj->eventoAuxList();
        while ($row = $obj->extraer_registro($result)) {
            $xtpl->assign('ID_EVENTO_AUX', $row[0]);
            $xtpl->assign('DES_EVENTO_AUX', $row[1]);
            $xtpl->assign('SELECTED_EVENTO_AUX', ($aRegist['id_evento_aux'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.evento_aux');
        }
        $obj->btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "convocatoria"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
