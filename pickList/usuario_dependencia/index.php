<?php {

    session_start();
    include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
    $xtpl = new XTemplate('view.html');
    $xtpl->assign('FDSOIL', $_SESSION['FDSoil']); 
    $xtpl->assign('APPORG', $_SESSION['appOrg']);
    include("../../../".$_SESSION['myApp']."/class/Minuta.model.php");
    $obj = new Minuta();    
    $classTR = 'lospare';
    $arr['where'] = 'LIST';
    $result = $obj->usuarioDependenciaGet($arr);
    while ($row = $obj->extraer_asociativo($result)) {
            $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row['id']);
	    $xtpl->assign('USUARIO', $row['nombre'].', '.$row['apellido']);
            $xtpl->assign('DEPENDENCIA', $row['dependencia']);
            $xtpl->parse('main.nivel_0');
        $classTR = ($classTR == 'lospare') ? 'losnone' : 'lospare';
    }
    $xtpl->parse('main');
    $xtpl->out('main');
}
