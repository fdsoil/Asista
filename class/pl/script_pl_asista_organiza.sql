-- Function: asista.organiza_register(integer, character varying, text)

-- DROP FUNCTION asista.organiza_register(integer, character varying, text);

CREATE OR REPLACE FUNCTION asista.organiza_register(i_id integer, i_nombre character varying, i_objeto text)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.organiza
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO asista.organiza(
                                nombre,
                                objeto)
                        VALUES (
                                i_nombre,
                                i_objeto);
                        SELECT max(id) INTO v_id FROM asista.organiza
                                WHERE nombre=i_nombre;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE asista.organiza SET
                        objeto=i_objeto
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.organiza_register(integer, character varying, text)
  OWNER TO postgres;

-- Function: asista.organiza_delete(integer)

-- DROP FUNCTION asista.organiza_delete(integer);

CREATE OR REPLACE FUNCTION asista.organiza_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.organiza WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.organiza_delete(integer)
  OWNER TO postgres;

-- Function: asista.organiza_red_social_register(integer, integer, integer, character varying)

-- DROP FUNCTION asista.organiza_red_social_register(integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION asista.organiza_red_social_register(i_id integer, i_id_organiza integer, i_id_red_social integer, i_red_social character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.organiza_red_social(
                        id_organiza,
                        id_red_social,
                        red_social)
                VALUES (
                        i_id_organiza,
                        i_id_red_social,
                        i_red_social);
                o_return:= 'C';
        ELSE
                UPDATE asista.organiza_red_social SET
                        id_organiza=i_id_organiza,
                        id_red_social=i_id_red_social,
                        red_social=i_red_social
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.organiza_red_social_register(integer, integer, integer, character varying)
  OWNER TO postgres;

-- Function: asista.organiza_red_social_delete(integer)

-- DROP FUNCTION asista.organiza_red_social_delete(integer);

CREATE OR REPLACE FUNCTION asista.organiza_red_social_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.organiza_red_social WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.organiza_red_social_delete(integer)
  OWNER TO postgres;

