-- Function: asista.sede_register(integer, character varying, character varying, character varying, character varying, character varying, text)

-- DROP FUNCTION asista.sede_register(integer, character varying, character varying, character varying, character varying, character varying, text);

CREATE OR REPLACE FUNCTION asista.sede_register(i_id integer, i_sede character varying, i_estado character varying, i_municipio character varying, i_ciudad character varying, i_parroquia character varying, i_direccion text)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.sede
                        WHERE sede=i_sede;
                IF  v_existe='f' THEN
                        INSERT INTO asista.sede(
                                sede,
                                estado,
                                municipio,
                                ciudad,
                                parroquia,
                                direccion)
                        VALUES (
                                i_sede,
                                i_estado,
                                i_municipio,
                                i_ciudad,
                                i_parroquia,
                                i_direccion);
                        SELECT max(id) INTO v_id FROM asista.sede
                                WHERE sede=i_sede;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE asista.sede SET
                        estado=i_estado,
                        municipio=i_municipio,
                        ciudad=i_ciudad,
                        parroquia=i_parroquia,
                        direccion=i_direccion
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.sede_register(integer, character varying, character varying, character varying, character varying, character varying, text)
  OWNER TO postgres;

-- Function: asista.sede_delete(integer)

-- DROP FUNCTION asista.sede_delete(integer);

CREATE OR REPLACE FUNCTION asista.sede_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.sede WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.sede_delete(integer)
  OWNER TO postgres;

-- Function: asista.sede_red_social_register(integer, integer, integer, character varying)

-- DROP FUNCTION asista.sede_red_social_register(integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION asista.sede_red_social_register(i_id integer, i_id_sede integer, i_id_red_social integer, i_red_social character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.sede_red_social(
                        id_sede,
                        id_red_social,
                        red_social)
                VALUES (
                        i_id_sede,
                        i_id_red_social,
                        i_red_social);
                o_return:= 'C';
        ELSE
                UPDATE asista.sede_red_social SET
                        id_sede=i_id_sede,
                        id_red_social=i_id_red_social,
                        red_social=i_red_social
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.sede_red_social_register(integer, integer, integer, character varying)
  OWNER TO postgres;

-- Function: asista.sede_red_social_delete(integer)

-- DROP FUNCTION asista.sede_red_social_delete(integer);

CREATE OR REPLACE FUNCTION asista.sede_red_social_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.sede_red_social WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.sede_red_social_delete(integer)
  OWNER TO postgres;

