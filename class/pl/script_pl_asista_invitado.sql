-- Function: asista.invitado_register(integer, character varying, character varying, character varying, character, character varying, character varying, character varying, character varying)

-- DROP FUNCTION asista.invitado_register(integer, character varying, character varying, character varying, character, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION asista.invitado_register(i_id integer, i_cedula character varying, i_nombre character varying, i_apellido character varying, i_sexo character, i_estado character varying, i_municipio character varying, i_ciudad character varying, i_parroquia character varying)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.invitado
                        WHERE cedula=i_cedula;
                IF  v_existe='f' THEN
                        INSERT INTO asista.invitado(
                                cedula,
                                nombre,
                                apellido,
                                sexo,
                                estado,
                                municipio,
                                ciudad,
                                parroquia)
                        VALUES (
                                i_cedula,
                                i_nombre,
                                i_apellido,
                                i_sexo,
                                i_estado,
                                i_municipio,
                                i_ciudad,
                                i_parroquia);
                        SELECT max(id) INTO v_id FROM asista.invitado
                                WHERE cedula=i_cedula;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE asista.invitado SET
                        nombre=i_nombre,
                        apellido=i_apellido,
                        sexo=i_sexo,
                        estado=i_estado,
                        municipio=i_municipio,
                        ciudad=i_ciudad,
                        parroquia=i_parroquia
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_register(integer, character varying, character varying, character varying, character, character varying, character varying, character varying, character varying)
  OWNER TO postgres;

-- Function: asista.invitado_delete(integer)

-- DROP FUNCTION asista.invitado_delete(integer);

CREATE OR REPLACE FUNCTION asista.invitado_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.invitado WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_delete(integer)
  OWNER TO postgres;

-- Function: asista.invitado_red_social_register(integer, integer, integer, character varying)

-- DROP FUNCTION asista.invitado_red_social_register(integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION asista.invitado_red_social_register(i_id integer, i_id_invitado integer, i_id_red_social integer, i_red_social character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.invitado_red_social(
                        id_invitado,
                        id_red_social,
                        red_social)
                VALUES (
                        i_id_invitado,
                        i_id_red_social,
                        i_red_social);
                o_return:= 'C';
        ELSE
                UPDATE asista.invitado_red_social SET
                        id_invitado=i_id_invitado,
                        id_red_social=i_id_red_social,
                        red_social=i_red_social
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_red_social_register(integer, integer, integer, character varying)
  OWNER TO postgres;

-- Function: asista.invitado_red_social_delete(integer)

-- DROP FUNCTION asista.invitado_red_social_delete(integer);

CREATE OR REPLACE FUNCTION asista.invitado_red_social_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.invitado_red_social WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_red_social_delete(integer)
  OWNER TO postgres;

-- Function: asista.invitado_ocupacion_register(integer, integer, integer)

-- DROP FUNCTION asista.invitado_ocupacion_register(integer, integer, integer);

CREATE OR REPLACE FUNCTION asista.invitado_ocupacion_register(i_id integer, i_id_invitado integer, i_id_ocupacion integer)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.invitado_ocupacion(
                        id_invitado,
                        id_ocupacion)
                VALUES (
                        i_id_invitado,
                        i_id_ocupacion);
                o_return:= 'C';
        ELSE
                UPDATE asista.invitado_ocupacion SET
                        id_invitado=i_id_invitado,
                        id_ocupacion=i_id_ocupacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_ocupacion_register(integer, integer, integer)
  OWNER TO postgres;

-- Function: asista.invitado_ocupacion_delete(integer)

-- DROP FUNCTION asista.invitado_ocupacion_delete(integer);

CREATE OR REPLACE FUNCTION asista.invitado_ocupacion_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.invitado_ocupacion WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.invitado_ocupacion_delete(integer)
  OWNER TO postgres;

