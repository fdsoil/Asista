-- Function: asista.ocupacion_register(integer, character varying)

-- DROP FUNCTION asista.ocupacion_register(integer, character varying);

CREATE OR REPLACE FUNCTION asista.ocupacion_register(i_id integer, i_descripcion character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.ocupacion
                        WHERE descripcion=i_descripcion;
                IF  v_existe='f' THEN
                        INSERT INTO asista.ocupacion(
                                descripcion)
                        VALUES (
                                i_descripcion);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE asista.ocupacion SET
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.ocupacion_register(integer, character varying)
  OWNER TO postgres;

-- Function: asista.ocupacion_delete(integer)

-- DROP FUNCTION asista.ocupacion_delete(integer);

CREATE OR REPLACE FUNCTION asista.ocupacion_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.ocupacion WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.ocupacion_delete(integer)
  OWNER TO postgres;

