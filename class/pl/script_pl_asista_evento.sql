-- Function: asista.evento_register(integer, character varying, date, character)

-- DROP FUNCTION asista.evento_register(integer, character varying, date, character);

CREATE OR REPLACE FUNCTION asista.evento_register(i_id integer, i_descripcion character varying, i_fecha_aux date, i_hora_aux character)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.evento
                        WHERE descripcion=i_descripcion;
                IF  v_existe='f' THEN
                        INSERT INTO asista.evento(
                                descripcion,
                                fecha_aux,
                                hora_aux)
                        VALUES (
                                i_descripcion,
                                i_fecha_aux,
                                i_hora_aux);
                        SELECT max(id) INTO v_id FROM asista.evento
                                WHERE descripcion=i_descripcion;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE asista.evento SET
                        fecha_aux=i_fecha_aux,
                        hora_aux=i_hora_aux
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_register(integer, character varying, date, character)
  OWNER TO postgres;

-- Function: asista.evento_delete(integer)

-- DROP FUNCTION asista.evento_delete(integer);

CREATE OR REPLACE FUNCTION asista.evento_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.evento WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_delete(integer)
  OWNER TO postgres;

-- Function: asista.evento_aux_register(integer, integer, integer, date, character)

-- DROP FUNCTION asista.evento_aux_register(integer, integer, integer, date, character);

CREATE OR REPLACE FUNCTION asista.evento_aux_register(i_id integer, i_id_evento integer, i_id_sede integer, i_fecha date, i_hora character)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.evento_aux(
                        id_evento,
                        id_sede,
                        fecha,
                        hora)
                VALUES (
                        i_id_evento,
                        i_id_sede,
                        i_fecha,
                        i_hora);
                o_return:= 'C';
        ELSE
                UPDATE asista.evento_aux SET
                        id_evento=i_id_evento,
                        id_sede=i_id_sede,
                        fecha=i_fecha,
                        hora=i_hora
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_aux_register(integer, integer, integer, date, character)
  OWNER TO postgres;

-- Function: asista.evento_aux_delete(integer)

-- DROP FUNCTION asista.evento_aux_delete(integer);

CREATE OR REPLACE FUNCTION asista.evento_aux_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.evento_aux WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_aux_delete(integer)
  OWNER TO postgres;

-- Function: asista.evento_organiza_register(integer, integer, integer)

-- DROP FUNCTION asista.evento_organiza_register(integer, integer, integer);

CREATE OR REPLACE FUNCTION asista.evento_organiza_register(i_id integer, i_id_evento integer, i_id_organiza integer)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO asista.evento_organiza(
                        id_evento,
                        id_organiza)
                VALUES (
                        i_id_evento,
                        i_id_organiza);
                o_return:= 'C';
        ELSE
                UPDATE asista.evento_organiza SET
                        id_evento=i_id_evento,
                        id_organiza=i_id_organiza
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_organiza_register(integer, integer, integer)
  OWNER TO postgres;

-- Function: asista.evento_organiza_delete(integer)

-- DROP FUNCTION asista.evento_organiza_delete(integer);

CREATE OR REPLACE FUNCTION asista.evento_organiza_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.evento_organiza WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.evento_organiza_delete(integer)
  OWNER TO postgres;

