-- Function: asista.programacion_register(integer, integer, date, time without time zone, character varying, character varying, character varying)

-- DROP FUNCTION asista.programacion_register(integer, integer, date, time without time zone, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION asista.programacion_register(i_id integer, i_id_evento_aux integer, i_fecha date, i_hora time without time zone, i_programa character varying, i_descripcion character varying, i_representante character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.programacion
                        WHERE id_evento_aux=i_id_evento_aux
                                        AND programa=i_programa;
                IF  v_existe='f' THEN
                        INSERT INTO asista.programacion(
                                id_evento_aux,
                                fecha,
                                hora,
                                programa,
                                descripcion,
                                representante)
                        VALUES (
                                i_id_evento_aux,
                                i_fecha,
                                i_hora,
                                i_programa,
                                i_descripcion,
                                i_representante);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE asista.programacion SET
                        fecha=i_fecha,
                        hora=i_hora,
                        descripcion=i_descripcion,
                        representante=i_representante
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.programacion_register(integer, integer, date, time without time zone, character varying, character varying, character varying)
  OWNER TO postgres;

-- Function: asista.programacion_delete(integer)

-- DROP FUNCTION asista.programacion_delete(integer);

CREATE OR REPLACE FUNCTION asista.programacion_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.programacion WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.programacion_delete(integer)
  OWNER TO postgres;

