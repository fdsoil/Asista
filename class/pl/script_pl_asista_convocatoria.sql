-- Function: asista.convocatoria_register(integer, integer, integer, date, time without time zone, boolean, date, time without time zone, boolean, date, time without time zone, text)

-- DROP FUNCTION asista.convocatoria_register(integer, integer, integer, date, time without time zone, boolean, date, time without time zone, boolean, date, time without time zone, text);

CREATE OR REPLACE FUNCTION asista.convocatoria_register(i_id integer, i_id_evento_aux integer, i_id_invitado integer, i_fecha_convocatoria date, i_hora_convocatoria time without time zone, i_confirmacion boolean, i_fecha_confirmacion date, i_hora_confirmacion time without time zone, i_asistencia boolean, i_fecha_asistencia date, i_hora_asistencia time without time zone, i_observacion text)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM asista.convocatoria
                        WHERE id_evento_aux=i_id_evento_aux
                                        AND id_invitado=i_id_invitado;
                IF  v_existe='f' THEN
                        INSERT INTO asista.convocatoria(
                                id_evento_aux,
                                id_invitado,
                                fecha_convocatoria,
                                hora_convocatoria,
                                confirmacion,
                                fecha_confirmacion,
                                hora_confirmacion,
                                asistencia,
                                fecha_asistencia,
                                hora_asistencia,
                                observacion)
                        VALUES (
                                i_id_evento_aux,
                                i_id_invitado,
                                i_fecha_convocatoria,
                                i_hora_convocatoria,
                                i_confirmacion,
                                i_fecha_confirmacion,
                                i_hora_confirmacion,
                                i_asistencia,
                                i_fecha_asistencia,
                                i_hora_asistencia,
                                i_observacion);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE asista.convocatoria SET
                        fecha_convocatoria=i_fecha_convocatoria,
                        hora_convocatoria=i_hora_convocatoria,
                        confirmacion=i_confirmacion,
                        fecha_confirmacion=i_fecha_confirmacion,
                        hora_confirmacion=i_hora_confirmacion,
                        asistencia=i_asistencia,
                        fecha_asistencia=i_fecha_asistencia,
                        hora_asistencia=i_hora_asistencia,
                        observacion=i_observacion
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.convocatoria_register(integer, integer, integer, date, time without time zone, boolean, date, time without time zone, boolean, date, time without time zone, text)
  OWNER TO postgres;

-- Function: asista.convocatoria_delete(integer)

-- DROP FUNCTION asista.convocatoria_delete(integer);

CREATE OR REPLACE FUNCTION asista.convocatoria_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM asista.convocatoria WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asista.convocatoria_delete(integer)
  OWNER TO postgres;

