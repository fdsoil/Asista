<?php

include_once('../../../'.$_SESSION['FDSoil'].'/class/Usuario.model.php');

class Ocupacion extends Usuario 
{

    /** Arreglo asociativo que contiene los campos y sus respectivas
    * característica. Dichos campos son los únicos valores permitidos
    * a traves de $_REQUEST, $_POST y $_GET para esta clase.*/
    private $aValReqs = array(
        "id" => array(
            "label" => "Id",
            "required" => false
        ),
        "descripcion" => array(
            "label" => "Descripcion",
            "required" => false
        )
    );

    private function path()
    {
        return '../../../'.$_SESSION['myApp'].'/class/sql/ocupacion/';
    }

    function ocupacionGet($arr)
    {
        switch ($arr['where']){
            case 'LIST':
                $arr['where']='';
                break;
            case 'REGIST':
                $arr['id']=$_POST['id'];
                $arr['where']=$this->replace_data($arr, ' WHERE id={fld:id} ');
                break;
        }
        return $this->exeQryFile($this->path().'ocupacion_get_select.sql', $arr);
    }

    function ocupacionRegister()
    {
        $aValReqs = $this->valReqs($_POST, $this->aValReqs);
        if (!$aValReqs){
            $_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'ocupacion_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
            $np = 1;
        }
        $this->adminMsj($msj,$np);
    }

    function ocupacionDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'ocupacion_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        if ($row[0]!='B')
            $obj->adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

}
