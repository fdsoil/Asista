<?php

include_once('../../../'.$_SESSION['FDSoil'].'/class/Usuario.model.php');

class Programacion extends Usuario 
{

    /** Arreglo asociativo que contiene los campos y sus respectivas
    * característica. Dichos campos son los únicos valores permitidos
    * a traves de $_REQUEST, $_POST y $_GET para esta clase.*/
    private $aValReqs = array(
        "id" => array(
            "label" => "Id",
            "required" => false
        ),
        "id_evento_aux" => array(
            "label" => "Id Evento Aux",
            "required" => false
        ),
        "fecha" => array(
            "label" => "Fecha",
            "required" => false
        ),
        "hora" => array(
            "label" => "Hora",
            "required" => false
        ),
        "programa" => array(
            "label" => "Programa",
            "required" => false
        ),
        "descripcion" => array(
            "label" => "Descripcion",
            "required" => false
        ),
        "representante" => array(
            "label" => "Representante",
            "required" => false
        )
    );

    private function path()
    {
        return '../../../'.$_SESSION['myApp'].'/class/sql/programacion/';
    }

    function programacionGet($arr)
    {
        switch ($arr['where']){
            case 'LIST':
                $arr['where']='';
                break;
            case 'REGIST':
                $arr['id']=$_POST['id'];
                $arr['where']=$this->replace_data($arr, ' WHERE A.id={fld:id} ');
                break;
        }
        return $this->exeQryFile($this->path().'programacion_get_select.sql', $arr);
    }

    function programacionRegister()
    {
        $aValReqs = $this->valReqs($_POST, $this->aValReqs);
        if (!$aValReqs){
            $_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'programacion_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
            $np = 1;
        }
        $this->adminMsj($msj,$np);
    }

    function programacionDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'programacion_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        if ($row[0]!='B')
            $obj->adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    function eventoAuxList()
    {
        return $this->exeQryFile($this->path().'evento_aux_list_select.sql', $_POST);
    }

}
