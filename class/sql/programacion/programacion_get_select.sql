SELECT 
    A.id,
    C.descripcion||' - '||B.fecha||' - '||D.sede||' - '||D.estado AS des_evento_aux,
    A.id_evento_aux,
    A.fecha AS fech_programa,
    A.hora AS hora_programa,
    A.programa,
    A.descripcion,
    A.representante
    --,B.id_evento 
    --,B.id_sede 
    --,B.fecha AS fech_evento_aux
    --,B.hora AS hora_evento_aux
FROM asista.programacion A
INNER JOIN asista.evento_aux B ON A.id_evento_aux=B.id
INNER JOIN asista.evento C ON B.id_evento=C.id
INNER JOIN asista.sede D ON B.id_sede=D.id
{fld:where}
ORDER BY 2;
