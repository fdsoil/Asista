SELECT A.id, B.descripcion||' - '||fecha||' - '||C.sede||' - '||C.estado AS descripcion, id_evento, id_sede, fecha, hora 
FROM asista.evento_aux A
INNER JOIN asista.evento B ON A.id_evento=B.id
INNER JOIN asista.sede C ON A.id_sede=C.id
ORDER BY 2;
