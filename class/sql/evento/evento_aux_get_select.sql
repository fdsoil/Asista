SELECT 
    A.id,
    A.id_evento,
    A.id_sede,
    A.fecha,
    A.hora
    ,B.sede AS des_sede
    ,B.estado AS des_estado
    ,B.municipio AS des_municipio
    ,B.ciudad AS des_ciudad
    ,B.parroquia AS des_parroquia
    ,B.direccion AS des_direccion
FROM asista.evento_aux A
INNER JOIN asista.sede B ON A.id_sede=B.id
WHERE A.id_evento = {fld:id}
ORDER BY 3;
