SELECT 
    A.id,
    A.id_evento,
    A.id_organiza
    ,B.nombre AS des_organiza
    ,B.objeto AS des_objeto
FROM asista.evento_organiza A
INNER JOIN asista.organiza B ON A.id_organiza=B.id
WHERE A.id_evento = {fld:id}
ORDER BY 3;
