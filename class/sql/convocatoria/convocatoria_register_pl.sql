SELECT asista.convocatoria_register(
    {fld:id},
    {fld:id_evento_aux},
    {fld:id_invitado},
    '{fld:fecha_convocatoria}',
    '{fld:hora_convocatoria}',
    '{fld:confirmacion}',
    '{fld:fecha_confirmacion}',
    '{fld:hora_confirmacion}',
    '{fld:asistencia}',
    '{fld:fecha_asistencia}',
    '{fld:hora_asistencia}',
    '{fld:observacion}'
);