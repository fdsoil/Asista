SELECT 
    A.id
    ,D.descripcion||' - '||B.fecha||' - '||E.sede||' - '||E.estado AS des_evento
    ,C.cedula||' - '||C.nombre||', '||C.apellido AS des_invitado,
    A.id_evento_aux,
    A.id_invitado,
    A.fecha_convocatoria,
    A.hora_convocatoria,
    A.confirmacion,
    A.fecha_confirmacion,
    A.hora_confirmacion,
    A.asistencia,
    A.fecha_asistencia,
    A.hora_asistencia,
    A.observacion
    ,B.id_evento 
    ,B.id_sede 
    ,B.fecha AS des_fecha_evento_aux
    ,B.hora AS des_hora__evento_aux
    ,C.cedula AS des_invitado_cedula
    ,C.nombre AS des_invitado_nombre
    ,C.apellido AS des_invitado_apellido
    ,C.sexo AS des_invitado_sexo
    ,C.estado AS des_invitado_estado
    ,C.municipio AS des_invitado_municipio
    ,C.ciudad AS des_invitado_ciudad
    ,C.parroquia AS des_invitado_parroquia
FROM asista.convocatoria A
INNER JOIN asista.evento_aux B ON A.id_evento_aux=B.id
INNER JOIN asista.invitado C ON A.id_invitado=C.id
INNER JOIN asista.evento D ON B.id_evento=D.id
INNER JOIN asista.sede E ON B.id_sede=E.id
{fld:where}
ORDER BY 2;

