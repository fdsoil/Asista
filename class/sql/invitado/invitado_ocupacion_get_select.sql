SELECT 
    A.id,
    A.id_invitado,
    A.id_ocupacion
    ,B.descripcion AS des_ocupacion
FROM asista.invitado_ocupacion A
INNER JOIN asista.ocupacion B ON A.id_ocupacion=B.id
WHERE A.id_invitado = {fld:id}
ORDER BY 3;
