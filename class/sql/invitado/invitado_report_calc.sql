SELECT A.id, nombre||', '||apellido AS invitado, estado, descripcion, red_social
  FROM asista.invitado A 
  INNER JOIN asista.invitado_red_social B ON A.id= B.id_invitado
  INNER JOIN asista.red_social C ON C.id=B.id_red_social
  ORDER BY 1;
