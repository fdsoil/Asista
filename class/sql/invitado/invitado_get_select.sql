SELECT 
    id,
    cedula,
    nombre,
    apellido,
    sexo,
    estado,
    municipio,
    ciudad,
    parroquia
FROM asista.invitado 
{fld:where}
ORDER BY 2;
