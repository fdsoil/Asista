SELECT 
    id,
    sede,
    estado,
    municipio,
    ciudad,
    parroquia,
    direccion
FROM asista.sede 
{fld:where}
ORDER BY 2;
