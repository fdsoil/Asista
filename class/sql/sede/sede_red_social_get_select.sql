SELECT 
    A.id,
    A.id_sede,
    A.id_red_social,
    A.red_social
    ,B.descripcion AS des_red_social
FROM asista.sede_red_social A
INNER JOIN asista.red_social B ON A.id_red_social=B.id
WHERE A.id_sede = {fld:id}
ORDER BY 3;
