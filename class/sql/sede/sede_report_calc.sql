SELECT A.id, sede, estado, descripcion, red_social
  FROM asista.sede A 
  INNER JOIN asista.sede_red_social B ON A.id= B.id_sede
  INNER JOIN asista.red_social C ON C.id=B.id_red_social
  ORDER BY 1;
