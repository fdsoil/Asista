SELECT 
    A.id,
    A.id_organiza,
    A.id_red_social,
    A.red_social
    ,B.descripcion AS des_red_social
FROM asista.organiza_red_social A
INNER JOIN asista.red_social B ON A.id_red_social=B.id
WHERE A.id_organiza = {fld:id}
ORDER BY 3;
