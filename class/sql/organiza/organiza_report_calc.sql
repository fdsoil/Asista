SELECT A.id, nombre, objeto, descripcion, red_social
  FROM asista.organiza A 
  INNER JOIN asista.organiza_red_social B ON A.id= B.id_organiza
  INNER JOIN asista.red_social C ON C.id=B.id_red_social
  ORDER BY 1;
