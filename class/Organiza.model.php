<?php

include_once('../../../'.$_SESSION['FDSoil'].'/class/Usuario.model.php');

class Organiza extends Usuario 
{

    /** Arreglo asociativo que contiene los campos y sus respectivas
    * característica. Dichos campos son los únicos valores permitidos
    * a traves de $_REQUEST, $_POST y $_GET para esta clase.*/
    private $aValReqs = array(
        "id" => array(
            "label" => "Id",
            "required" => false
        ),
        "nombre" => array(
            "label" => "Nombre",
            "required" => false
        ),
        "objeto" => array(
            "label" => "Objeto",
            "required" => false
        )
    );

    private function path()
    {
        return '../../../'.$_SESSION['myApp'].'/class/sql/organiza/';
    }

    function organizaGet($arr)
    {
        switch ($arr['where']){
            case 'LIST':
                $arr['where']='';
                break;
            case 'REGIST':
                $arr['id']=$_POST['id'];
                $arr['where']=$this->replace_data($arr, ' WHERE id={fld:id} ');
                break;
        }
        return $this->exeQryFile($this->path().'organiza_get_select.sql', $arr);
    }

    function organizaRegister()
    {
        $aValReqs = $this->valReqs($_POST, $this->aValReqs);
        if (!$aValReqs){
            $_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'organiza_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
        }
        return $msj;
    }

    function organizaDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'organiza_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        if ($row[0]!='B')
            $obj->adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    function redSocialList()
    {
        return $this->exeQryFile($this->path().'red_social_list_select.sql', $_POST);
    }

    function organizaRedSocialRegister()
    {
        //$aValReqs = $this->valReqs($_POST, $this->aValReqs);
        //if (!$aValReqs) {
            //$_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'organiza_red_social_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
        //} else {
            //$_SESSION['messages'] = $aValReqs;
            //$msj = 'N';
        //}
        return $msj;
    }

    function organizaRedSocialGet()
    {
        return $this->getMatrixAsociativoQryFile($this->path().'organiza_red_social_get_select.sql', $_POST);
    }

    function organizaRedSocialDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'organiza_red_social_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        return $row[0];
    }

    function organizaReportCalc()
    {
        $matrix = $this->getMatrixRegistroQryFile($this->path().'organiza_report_calc.sql', $_POST);
        return $this->organizaReportCalcAux($matrix);
    }

    private function organizaReportCalcAux($aOld)
    {
        $aNew[0][0] = [ $aOld[0][1] , $aOld[0][2] ];
        $id = $aOld[0][0];
        $j = 0;
        $k = 0;
        for ( $i = 0 ; $i < count($aOld) ; $i++ ) {
            if ( $id == $aOld[$i][0] )
                $aNew[$j][1][$k++]  = $aOld[$i][3].' : '.$aOld[$i][4];
            else {
                $k = 0;
                $id = $aOld[$i][0];
                $aNew[++$j][0] = [ $aOld[$i][1] , $aOld[$i--][2] ];
            }
        }
        return $aNew;
    }

}
