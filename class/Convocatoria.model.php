<?php

include_once('../../../'.$_SESSION['FDSoil'].'/class/Usuario.model.php');

class Convocatoria extends Usuario 
{

    /** Arreglo asociativo que contiene los campos y sus respectivas
    * característica. Dichos campos son los únicos valores permitidos
    * a traves de $_REQUEST, $_POST y $_GET para esta clase.*/
    private $aValReqs = array(
        "id" => array(
            "label" => "Id",
            "required" => false
        ),
        "id_evento_aux" => array(
            "label" => "Id Evento Aux",
            "required" => false
        ),
        "id_invitado" => array(
            "label" => "Id Invitado",
            "required" => false
        ),
        "fecha_convocatoria" => array(
            "label" => "Fecha Convocatoria",
            "required" => false
        ),
        "hora_convocatoria" => array(
            "label" => "Hora Convocatoria",
            "required" => false
        ),
        "confirmacion" => array(
            "label" => "Confirmacion",
            "required" => false
        ),
        "fecha_confirmacion" => array(
            "label" => "Fecha Confirmacion",
            "required" => false
        ),
        "hora_confirmacion" => array(
            "label" => "Hora Confirmacion",
            "required" => false
        ),
        "asistencia" => array(
            "label" => "Asistencia",
            "required" => false
        ),
        "fecha_asistencia" => array(
            "label" => "Fecha Asistencia",
            "required" => false
        ),
        "hora_asistencia" => array(
            "label" => "Hora Asistencia",
            "required" => false
        ),
        "observacion" => array(
            "label" => "Observacion",
            "required" => false
        )
    );

    private function path()
    {
        return '../../../'.$_SESSION['myApp'].'/class/sql/convocatoria/';
    }

    function convocatoriaGet($arr)
    {
        switch ($arr['where']){
            case 'LIST':
                $arr['where']='';
                break;
            case 'REGIST':
                $arr['id']=$_POST['id'];
                $arr['where']=$this->replace_data($arr, ' WHERE A.id={fld:id} ');
                break;
        }
        return $this->exeQryFile($this->path().'convocatoria_get_select.sql', $arr);
    }

    function convocatoriaRegister()
    {
        $_POST['confirmacion'] = array_key_exists("confirmacion",$_POST) ? 't' : 'f';
        $_POST['asistencia'] = array_key_exists("asistencia",$_POST) ? 't' : 'f';
        $aValReqs = $this->valReqs($_POST, $this->aValReqs);
        if (!$aValReqs){
            $_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'convocatoria_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
            $np = 1;
        }
        $this->adminMsj($msj,$np);
    }

    function convocatoriaDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'convocatoria_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        if ($row[0]!='B')
            $obj->adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    function eventoAuxList()
    {
        return $this->exeQryFile($this->path().'evento_aux_list_select.sql', $_POST);
    }

    function invitadoList()
    {
        return $this->exeQryFile($this->path().'invitado_list_select.sql', $_POST);
    }

    function invitadoGet()
    {
        return $this->exeQryFile($this->path().'invitado_get_select.sql', $_POST);
    }

}
