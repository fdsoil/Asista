<?php

include_once('../../../'.$_SESSION['FDSoil'].'/class/Usuario.model.php');

class Evento extends Usuario 
{

    /** Arreglo asociativo que contiene los campos y sus respectivas
    * característica. Dichos campos son los únicos valores permitidos
    * a traves de $_REQUEST, $_POST y $_GET para esta clase.*/
    private $aValReqs = array(
        "id" => array(
            "label" => "Id",
            "required" => false
        ),
        "descripcion" => array(
            "label" => "Descripcion",
            "required" => false
        ),
        "fecha_aux" => array(
            "label" => "Fecha Aux",
            "required" => false
        ),
        "hora_aux" => array(
            "label" => "Hora Aux",
            "required" => false
        )
    );

    private function path()
    {
        return '../../../'.$_SESSION['myApp'].'/class/sql/evento/';
    }

    function eventoGet($arr)
    {
        switch ($arr['where']){
            case 'LIST':
                $arr['where']='';
                break;
            case 'REGIST':
                $arr['id']=$_POST['id'];
                $arr['where']=$this->replace_data($arr, ' WHERE id={fld:id} ');
                break;
        }
        return $this->exeQryFile($this->path().'evento_get_select.sql', $arr);
    }

    function eventoRegister()
    {
        $aValReqs = $this->valReqs($_POST, $this->aValReqs);
        if (!$aValReqs){
            $_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'evento_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
        }
        return $msj;
    }

    function eventoDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'evento_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        if ($row[0]!='B')
            $obj->adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    function sedeList()
    {
        return $this->exeQryFile($this->path().'sede_list_select.sql', $_POST);
    }

    function organizaList()
    {
        return $this->exeQryFile($this->path().'organiza_list_select.sql', $_POST);
    }

    function eventoAuxRegister()
    {
        //$aValReqs = $this->valReqs($_POST, $this->aValReqs);
        //if (!$aValReqs) {
            //$_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'evento_aux_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
        //} else {
            //$_SESSION['messages'] = $aValReqs;
            //$msj = 'N';
        //}
        return $msj;
    }

    function eventoAuxGet()
    {
        return $this->getMatrixAsociativoQryFile($this->path().'evento_aux_get_select.sql', $_POST);
    }

    function eventoAuxDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'evento_aux_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        return $row[0];
    }

    function eventoOrganizaRegister()
    {
        //$aValReqs = $this->valReqs($_POST, $this->aValReqs);
        //if (!$aValReqs) {
            //$_POST = $this->formatReqs($_POST, $this->aValReqs);
            $row = $this->exeQryFileRegistro($this->path().'evento_organiza_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO');
            $msj = $row[0];
        //} else {
            //$_SESSION['messages'] = $aValReqs;
            //$msj = 'N';
        //}
        return $msj;
    }

    function eventoOrganizaGet()
    {
        return $this->getMatrixAsociativoQryFile($this->path().'evento_organiza_get_select.sql', $_POST);
    }

    function eventoOrganizaDelete()
    {
        $row = $this->exeQryFileRegistro($this->path().'evento_organiza_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO');
        return $row[0];
    }

}
