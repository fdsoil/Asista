<?php class Index
{
    public function main()
    {
        session_start();
        $strFileName = "config/app.json";
        file_exists($strFileName) ? $oJSON = json_decode(file_get_contents($strFileName)) : die("File nor Found " . $strFileName);
        require_once (__DIR__."/../".$oJSON->app->FDSoil."/class/Functions.class.php"); 
        $_SESSION=Functions::pasPropOfObjToArr( $oJSON, array('app'));
        header("Location: ../".strtolower($_SESSION['FDSoil'])."/admin_acceso/");
    }
}
Index::main();
