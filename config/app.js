var FDSoil='System';
var appOrg='Control';
var myApp='Asiste';
var modulos='modulos';
var Index='front';

function app() {
    this.name='A S I S T A';     
}

function appAcceso(){
    this.email='miaplicacion@midominio.com';
    this.telefon='(0000)-555-55-55';
    this.webUserRow=['','none', ''];
    this.loginType=[4, 'none'];    //1=>Número de Rif;
                                   //2=>Cédula/Pasaporte;
                                   //3=>Correo Institucional;
                                   //4=>Nombre de Usuario
    this.keypad=false;
    this.captcha=false;      
    this.popUp={ show: false, 
                 timeSleep: 700, 
                 info: "../../../../../"+myApp+"/img/popUpInfo.png", 
                 width: 221, 
                 height: 204 };
}

function appPasswrd(){
    this.rangeMin=[6];  //Rango mínimo de una contraseña;
    this.rangeMax=[10]; //Rango máximo de una contraseña;
    this.bUpp=false;    //Mínimo una letra mayúscula;
    this.bLow=false;    //Mínimo una letra minúscula;
}

