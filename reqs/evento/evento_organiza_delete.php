<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Evento.model.php');
        $obj = new Evento();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        echo base64_encode($obj->eventoOrganizaDelete());
    }
}
Index::execute();