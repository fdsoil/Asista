<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Evento.model.php');
        $obj = new Evento();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->eventoGet($arr))
                : $obj->iniRegist('evento','asista')
        );
    }
}
Index::execute();