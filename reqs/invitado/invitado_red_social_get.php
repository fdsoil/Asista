<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Invitado.model.php');
        $obj = new Invitado();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        echo base64_encode(json_encode($obj->invitadoRedSocialGet()));
    }
}
Index::execute();