<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Invitado.model.php');
        $obj = new Invitado();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->invitadoGet($arr))
                : $obj->iniRegist('invitado','asista')
        );
    }
}
Index::execute();