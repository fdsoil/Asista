<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Invitado.model.php');
        $obj = new Invitado();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        $_POST['cedula'] = $_POST['nacionalidad'].$_POST['cedula'];
        unset($_POST['nacionalidad']);
        self::lugarGeo();
        echo base64_encode($obj->invitadoRegister());
    }

    private function lugarGeo()
    {
        $arr = json_decode($_POST['id_lugar_geo']);
        unset($_POST['id_lugar_geo']);
        unset($_POST['lugar_geo']);
        $_POST['estado']    = $arr[0][1];
        $_POST['municipio'] = $arr[1][1];
        $_POST['ciudad']    = $arr[2][1];
        $_POST['parroquia'] = $arr[3][1];
    }

}
Index::execute();
