<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Minuta.model.php');
        $obj = new Minuta();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->minutaGet($arr))
                : $obj->iniRegist('minuta','minutas')
        );
    }
}
Index::execute();