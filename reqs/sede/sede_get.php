<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Sede.model.php');
        $obj = new Sede();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->sedeGet($arr))
                : $obj->iniRegist('sede','asista')
        );
    }
}
Index::execute();