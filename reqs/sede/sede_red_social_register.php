<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Sede.model.php');
        $obj = new Sede();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        echo base64_encode($obj->sedeRedSocialRegister());
    }
}
Index::execute();