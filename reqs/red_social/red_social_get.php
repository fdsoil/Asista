<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/RedSocial.model.php');
        $obj = new RedSocial();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->redSocialGet($arr))
                : $obj->iniRegist('red_social','asista')
        );
    }
}
Index::execute();