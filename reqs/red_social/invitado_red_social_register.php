<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/RedSocial.model.php');
        $obj = new RedSocial();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        echo base64_encode($obj->invitadoRedSocialRegister());
    }
}
Index::execute();