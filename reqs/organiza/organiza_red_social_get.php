<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Organiza.model.php');
        $obj = new Organiza();
        $_POST = $obj->base64DecodeArrValKey($_POST);
        echo base64_encode(json_encode($obj->organizaRedSocialGet()));
    }
}
Index::execute();