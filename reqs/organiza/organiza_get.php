<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Organiza.model.php');
        $obj = new Organiza();
        $arr['where'] = 'REGIST';
        echo json_encode(
            $_POST['id'] != 0
                ? $obj->extraer_asociativo($obj->organizaGet($arr))
                : $obj->iniRegist('organiza','asista')
        );
    }
}
Index::execute();