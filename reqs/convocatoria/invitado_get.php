<?php
class Index
{
    public function execute()
    {
        session_start();
        header('content-type: text/html; charset: utf-8');
        include_once('../../class/Convocatoria.model.php');
        $obj = new Convocatoria();
        echo json_encode($obj->extraer_asociativo($obj->invitadoGet($arr)));
    }
}
Index::execute();
