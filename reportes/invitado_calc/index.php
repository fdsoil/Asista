<?php class Index
{
    public function execute()
    {
        session_start();
        header('Content-type: text/html; charset=utf-8');
        header("Content-Type: application/vnd.ms-excel");
        require_once("../../../".$_SESSION['myApp']."/class/Invitado.model.php");
        $obj = new Invitado();
        $arr = $obj->invitadoReportCalc();        
        $tab='<table border=0><tr><th>Invitado</th><th>Redes Sociales</th></tr>';
        for ( $i = 0 ; $i < count($arr) ; $i++ ) {
            $tab.='<tr><td>'.$arr[$i][0][0].'</td><td>';
            for ( $j = 0 ; $j < count($arr[$i][1]) ; $j++ ) 
                $tab.=$arr[$i][1][$j].'<br/>';            
            $tab.='</td></tr>';
        }
        echo $tab.'</table>';
    }
}
Index::execute();
